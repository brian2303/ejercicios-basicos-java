package EjercicioDos;

import java.util.Scanner;

public class Ejercicio2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite el primer valor: ");
        int valueOne = sc.nextInt();
        System.out.println("Digite el segundo valor");
        int valueTwo = sc.nextInt();
        if(valueOne > valueTwo){
            System.out.println(valueOne + " Es mayor a " + valueTwo);
        }else if(valueTwo > valueOne){
            System.out.println(valueTwo + " Es mayor a " + valueOne);
        }else{
            System.out.println(valueOne + " Es igual a " + valueTwo);
        }
    }
}
