package EjercicioCatorce;

import java.util.Scanner;

public class Ejercicio14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite un numero");
        Integer numero = sc.nextInt();
        while (numero <= 1000){
            if(numero >= 1001) break;
            System.out.println(numero);
            numero += 2;
        }
    }
}
