package EjercicioTrece;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Ejercicio13 {
    public static void main(String[] args) {
        Date fecha = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd - HH:mm:ss");
        String fechaTexto = formatter.format(fecha);
        System.out.println("La fecha es: " + fechaTexto);
    }
}
