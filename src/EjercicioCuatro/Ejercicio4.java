package EjercicioCuatro;

import java.util.Scanner;

public class Ejercicio4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        final Double IVA = 0.21;
        System.out.println("Digite el precio del producto");
        Double precioProducto = sc.nextDouble();
        Double precioFinal = precioProducto + (precioProducto * IVA);
        System.out.println("El precio final del producto es: " + precioFinal);
    }
}
