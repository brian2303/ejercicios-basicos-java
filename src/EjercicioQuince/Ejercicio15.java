package EjercicioQuince;

import java.util.Scanner;

public class Ejercicio15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Integer opcion = 0;
        do {
            String menu = "****** GESTION CINEMATOGRÁFICA ******** \n" +
                    "1-NUEVO ACTOR \n" +
                    "2-BUSCAR ACTOR \n" +
                    "3-ELIMINAR ACTOR \n" +
                    "4-MODIFICAR ACTOR \n" +
                    "5-VER TODOS LOS ACTORES \n" +
                    "6-VER PELICULAS DE LOS ACTORES \n" +
                    "7-VER CATEGORIA DE LAS PELICULAS DE LOS ACTORES \n" +
                    "8-SALIR";
            System.out.println(menu);
            System.out.println("Digite una opcion:");
            opcion = sc.nextInt();
        }while (opcion != 8);

    }
}
