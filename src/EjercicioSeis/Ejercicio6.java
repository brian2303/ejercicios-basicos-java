package EjercicioSeis;

public class Ejercicio6 {
    public static void main(String[] args) {
        int numeroLimite = 100;
        for (int i = 0;i<=numeroLimite;i++){
            if(i % 2 == 0){
                System.out.println(i + " es par");
            }else{
                System.out.println(i + " es impar");
            }
        }
    }
}
