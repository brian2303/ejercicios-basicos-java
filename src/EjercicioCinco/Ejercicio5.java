package EjercicioCinco;

public class Ejercicio5 {
    public static void main(String[] args) {
        int numeroBase = 0;
        while(numeroBase <= 100){
            if(numeroBase % 2 == 0){
                System.out.println(numeroBase + " es par");
            }else{
                System.out.println(numeroBase + " es impar");
            }
            numeroBase++;
        }
    }
}
