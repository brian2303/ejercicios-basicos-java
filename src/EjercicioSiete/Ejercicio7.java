package EjercicioSiete;

import java.util.Scanner;

public class Ejercicio7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Integer numero;
        do{
            System.out.println("Digita un numero:");
            numero = sc.nextInt();
        }while (numero <= 0);
        System.out.println("El numero digitado fue: " + numero);
    }
}
