package EjercicioOnce;

import java.util.Locale;
import java.util.Scanner;

public class Ejercicio11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Integer contador = 0;
        String frase = sc.nextLine();
        frase = frase.toLowerCase();
        for (int i =0;i < frase.length();i++){
            if(frase.charAt(i) == 'a' || frase.charAt(i) == 'e' || frase.charAt(i) == 'i'
                || frase.charAt(i) == 'o' || frase.charAt(i) == 'u'){
                contador++;
            }
        }
        System.out.println("La cantidad de vocales es:" + contador);
        System.out.println("La longitud de la frase es:" + frase.length());
    }
}
