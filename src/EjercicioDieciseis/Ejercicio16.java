package EjercicioDieciseis;

import java.util.Scanner;

public class Ejercicio16 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingresa el nombre: ");
        String nombre = sc.nextLine();
        System.out.println("Ingresa la edad");
        Integer edad = sc.nextInt();
        System.out.println("Ingrese el sexo");
        char sexo = sc.next().charAt(0);
        System.out.println("Ingresa el peso");
        Double peso = sc.nextDouble();
        System.out.println("Ingresa la altura");
        Double altura = sc.nextDouble();
        Persona brayan = new Persona(nombre,edad,sexo,peso,altura);
        Persona andrea = new Persona(nombre,edad,sexo);
        andrea.setPeso(75.4);
        andrea.setAltura(1.75);
        Persona daniela = new Persona();
        daniela.setNombre("Daniela");
        daniela.setEdad(30);
        daniela.setSexo('M');
        daniela.setPeso(80.0);
        daniela.setAltura(1.67);
        comprobarPeso(brayan.calcularIMC());
        comprobarPeso(andrea.calcularIMC());
        comprobarPeso(daniela.calcularIMC());
        System.out.println(brayan.toString());
        System.out.println(andrea.toString());
        System.out.println(daniela.toString());
    }

    private static  void comprobarPeso(Integer result){
        if (result == -1){
            System.out.println("Por debajo del peso");
        }else if(result == 1){
            System.out.println("Peso ideal");
        }else{
            System.out.println("Sobrepeso");
        }
    }
}
