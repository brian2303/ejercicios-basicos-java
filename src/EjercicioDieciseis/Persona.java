package EjercicioDieciseis;

public class Persona {
    private String nombre = "";
    private Integer edad = 0;
    private String DNI;
    private char sexo = 'H';
    private Double peso = 0.0;
    private Double altura = 0.0;

    public void esMayorDeEdad(){
        if (this.edad >= 18){
            System.out.println("Es mayor de edad");
        }else{
            System.out.println("Es menor de edad");
        }
    }

    public void comprobarSexo(char sexo) {
        if(sexo != 'H' || sexo != 'M'){
            System.out.println("Sexo no valido");
        }else{
            this.sexo = sexo;
        }
    }

    public Integer calcularIMC(){
        if(this.peso / Math.pow(this.altura,2) < 20){
            return -1;
        }else if(this.peso / Math.pow(this.altura,2) >= 20 && this.peso / Math.pow(this.altura,2) <= 25){
            return 0;
        }else{
             return 1;
        }
    }

    private String generaDNI(){
        String DNI = "";
        for (int i = 0; i<= 8;i++){
            DNI +=  String.valueOf(Math.floor(Math.random() * 10)) ;
        }
        DNI += "A";
        return DNI;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "nombre='" + nombre + '\'' +
                ", edad=" + edad +
                ", DNI='" + DNI + '\'' +
                ", sexo=" + sexo +
                '}';
    }

    public Persona() {
        this.DNI = generaDNI();
    }

    public Persona(String nombre, Integer edad,char sexo) {
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
        this.DNI = generaDNI();
    }

    public Persona(String nombre, Integer edad, char sexo, Double peso, Double altura) {
        this.nombre = nombre;
        this.edad = edad;
        this.DNI = generaDNI();
        this.sexo = sexo;
        this.peso = peso;
        this.altura = altura;
    }

    public Double getPeso() {
        return peso;
    }


    public void setPeso(Double peso) {
        this.peso = peso;
    }

    public Double getAltura() {
        return altura;
    }

    public void setAltura(Double altura) {
        this.altura = altura;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getDNI() {
        return DNI;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo){
        this.sexo = sexo;
    }
}
