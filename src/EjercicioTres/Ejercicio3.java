package EjercicioTres;

import java.util.Scanner;

public class Ejercicio3 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite el radio: ");
        Double radio = sc.nextDouble();
        Double area = (Math.PI * Math.pow(radio,2));
        System.out.println("El area es: " + area);
    }
}
